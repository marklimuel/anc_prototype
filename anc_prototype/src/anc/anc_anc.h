
#ifndef ANC_ANC_H_
#define ANC_ANC_H_

#include "anc/anc_adc.h"
#include "anc/anc_dacc.h"

#define FXLMS_BUF_LEN	5
#define FXLMS_STEP		0.000000000005
//0.000000000001
#define FXLMS_MID		2048

struct {
	uint16_t x_idx;
	uint16_t x_prime_idx;
	
	float step;
	
	float x[FXLMS_BUF_LEN];
	float W[FXLMS_BUF_LEN];
	float x_prime[FXLMS_BUF_LEN];
	uint16_t val;
	int16_t inc;
	
} anc_fxlms;

void initialize_anc(void);
uint16_t anc_predict( uint16_t noise );
void anc_update( uint16_t residual );

#endif 